#! /usr/bin/env rdmd

import std.exception;
import std.stdio;
import std.file;
import std.process;

void main (string[] arguments) {
  
  enforce(arguments.length == 2);
  auto pythonCode = cast(string) std.file.read(arguments[1]);
  
  auto dCode = generate(tokenize(pythonCode));
  
  auto tempPath = std.file.tempDir ~ "/python_compiler_h1af3ijejivxkpqrduifgjbzss5gcfg37ks7bjnrluzyro5f";
  if (std.file.exists(tempPath))
    rmdirRecurse(tempPath);
  mkdirRecurse(tempPath);
  std.file.write(tempPath ~ "/program.d", dCode);
  
  //auto cwd = getcwd();
  //chdir(tempPath);
  //scope(exit) chdir(cwd);
  
  system("dmd -run " ~ tempPath ~ "/program.d");

}

enum tokenType {
  whitespace,
  symbol,
  literal,
}

struct token {
  tokenType type;
  string value;
}

token[] tokenize (string input) {
  
  import std.string;
  import std.regex;

  token[] tokens;
  int offset = 0;
  string remaining = input;
  
  while (remaining.length > 0) {
    
    if (remaining.length > stripLeft(remaining).length) {
      tokens ~= token(tokenType.whitespace, remaining[0 .. remaining.length - stripLeft(remaining).length]);
      remaining = remaining[remaining.length - stripLeft(remaining).length .. $];
      continue;
    }

    auto symbolRegex = ctRegex!r"^[a-z_][a-z0-9_]*";
    auto m = match(remaining, symbolRegex);
    if (m.captures.length > 0) {
      tokens ~= token(tokenType.symbol, m.captures[0]);
      remaining = remaining[m.captures[0].length .. $];
      continue;
    }

    auto literalRegex = ctRegex!`^"(\\\\.|[^\\\\"])*"`;
    auto m2 = match(remaining, literalRegex);
    if (m2.captures.length > 0) {
      tokens ~= token(tokenType.literal, m2.captures[0]);
      remaining = remaining[m2.captures[0].length .. $];
      continue;
    }
 
    throw new Exception("Syntax error");
  }

  //writeln(remaining);
  
  return tokens;
}


string generate (token[] tokens) {
  int offset = 0;
  string dCode = "import std.stdio;\n\nvoid main () {\n";
  
  for (offset = 0; offset < tokens.length; offset++) {
    auto token = tokens[offset];
    if (token.type == tokenType.whitespace)
      continue;
    if (token.type == tokenType.symbol) {
      if (token.value == "print") {
        if (tokens[offset + 1].type == tokenType.whitespace)
          offset++;
        assert(tokens[offset + 1].type == tokenType.literal);
        dCode ~= "writeln(" ~ tokens[offset + 1].value ~ ");\n";
        offset++;
      }
    } else {
      throw new Exception("Can't handle token");
    }

  }
  
  dCode ~= "\n}\n";

  return dCode;
}


